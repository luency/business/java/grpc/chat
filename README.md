[<img src="https://www.holstein-kiel.de/wp-content/uploads/contao_import/sponsoren/logos/dve_Wortmarke_anthrazit(1).png" alt="drawing" width="500"></img>](https://grpc.io/)

# Chat
Chat provides the opportunity to communicate with high performance through [**gRPC**](https://grpc.io/).
It's a public chat which can be used by everyone. Private chats might follow.

# Includes
* [**Chat - Client**](https://gitlab.com/luency/business/java/grpc/chat/tree/develop/chat-client)
* [**Chat - Proto**](https://gitlab.com/luency/business/java/grpc/chat/tree/develop/chat-proto)
* [**Chat - Service**](https://gitlab.com/luency/business/java/grpc/chat/tree/develop/chat-service)

# Installation
This part will follow, because this repo is just an internal one and just for development purposes.
Nothing is used in production.

# Additions
This project hasn't been finished yet and should not be used in production.
It was just used for development purposes and testing. JUnit tests will follow soon.