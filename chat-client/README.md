[<img src="https://www.holstein-kiel.de/wp-content/uploads/contao_import/sponsoren/logos/dve_Wortmarke_anthrazit(1).png" alt="drawing" width="500"></img>](https://grpc.io/)

# [Chat](https://gitlab.com/luency/business/java/grpc/chat/) - Client
This client is the user's application, so a user just has to be aware of this.
It connects to the [**gRPC**](https://grpc.io/) service included in the [master](https://gitlab.com/luency/business/java/grpc/chat/tree/develop/chat-service).
In addition, this instance also makes calls on the master.
Each client has to establish its own connection to the master.