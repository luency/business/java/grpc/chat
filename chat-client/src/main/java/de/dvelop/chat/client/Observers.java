package de.dvelop.chat.client;

import java.util.function.Consumer;

import io.grpc.stub.StreamObserver;

public final class Observers {
  private Observers() {}

  public static <T> StreamObserver<T> ignoreCompletion(
    Consumer<T> consumer,
    Consumer<Throwable> failureConsumer
  ) {
    return new StreamObserver<T>() {
      @Override
      public void onNext(T response) {
        consumer.accept(response);
      }

      @Override
      public void onError(Throwable throwable) {
        failureConsumer.accept(throwable);
      }

      @Override
      public void onCompleted() {
      }
    };
  }

  public static <T> StreamObserver<T> create(
    Consumer<T> consumer,
    Consumer<Throwable> failureConsumer,
    Runnable onCompletion
  ) {
    return new StreamObserver<T>() {
      @Override
      public void onNext(T response) {
        consumer.accept(response);
      }

      @Override
      public void onError(Throwable failure) {
        failureConsumer.accept(failure);
      }

      @Override
      public void onCompleted() {
        onCompletion.run();
      }
    };
  }
}