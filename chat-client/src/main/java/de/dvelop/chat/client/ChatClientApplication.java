package de.dvelop.chat.client;

import com.google.common.base.Preconditions;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import de.dvelop.chat.*;
import de.dvelop.chat.client.chat.ChatController;

import io.grpc.internal.DnsNameResolverProvider;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public final class ChatClientApplication extends Application {

  private static final int PORT = 8989;
  private static final String HOST = "10.3.4.138";
  private static final String LOGIN_PATH = "login.fxml";

  private static String currentName;
  private static ChatServiceGrpc.ChatServiceStub serviceStub;

  public static void main(String[] args) {
    launch(args);
  }

  private static ChatController chats;

  public static void changeCurrentName(String currentName) {
    Preconditions.checkNotNull(currentName);
    ChatClientApplication.currentName = currentName;
  }

  public static String currentName() {
    return currentName;
  }

  @Override
  public void start(Stage primaryStage) throws IOException, LineUnavailableException, UnsupportedAudioFileException {
    configureAndOpenLoginStage(primaryStage);
    connectToService();
    chats = new ChatController(primaryStage);
  }

  private void configureAndOpenLoginStage(Stage primaryStage) throws IOException {
    URL url = new File(LOGIN_PATH).toURI().toURL();
    Parent root = FXMLLoader.load(url);
    primaryStage.setTitle("d.velop ChatService");
    primaryStage.setScene(new Scene(root, 300, 400));
    primaryStage.setResizable(false);
    primaryStage.show();
  }

  private void connectToService() {
    ManagedChannel channel = createChannel();
    serviceStub = ChatServiceGrpc.newStub(channel);
  }

  public static ChatController chats() {
    return chats;
  }

  public static ChatServiceGrpc.ChatServiceStub serviceStub() {
    return serviceStub;
  }

  private ManagedChannel createChannel() {
    return ManagedChannelBuilder.forAddress(HOST, PORT)
      .usePlaintext()
      .enableFullStreamDecompression()
      .enableRetry()
      .nameResolverFactory(DnsNameResolverProvider.asFactory())
      .keepAliveWithoutCalls(true)
      .build();
  }
}