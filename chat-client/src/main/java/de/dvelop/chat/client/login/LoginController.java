package de.dvelop.chat.client.login;

import java.util.function.Consumer;

import javafx.application.Platform;
import javafx.fxml.FXML;

import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

import de.dvelop.chat.*;
import de.dvelop.chat.client.ChatClientApplication;
import de.dvelop.chat.client.Observers;
import io.grpc.stub.StreamObserver;

public final class LoginController {

  @FXML
  private PasswordField passwordField;

  @FXML
  private TextField usernameField;

  @FXML
  private Text statusMessage;

  @FXML
  private Button loginButton;

  @FXML
  private Button signUpButton;

  @FXML
  private void login() {
    if (invalidTextField(usernameField) || invalidTextField(passwordField)) {
      return;
    }
    String password = passwordField.getText();
    String username = usernameField.getText();

    UserLoginRequest request = createLoginRequest(username, password);
    ChatClientApplication.serviceStub().login(request, createLoginRequestObserver(username));
  }

  private StreamObserver<UserLoginResponse> createLoginRequestObserver(String username) {
    Consumer<UserLoginResponse> responseConsumer = createLoginResponseConsumer(username);
    return Observers.ignoreCompletion(responseConsumer, Throwable::printStackTrace);
  }

  private Consumer<UserLoginResponse> createLoginResponseConsumer(String username) {
    return loginResponse -> {
      if (loginResponse.getErrorState() == ErrorState.SUCCESS) {
        ChatClientApplication.changeCurrentName(username);
        ChatClientApplication.chats().open();
      } else if (loginResponse.getErrorState() == ErrorState.ERROR) {
        displayLoginError();
      }
    };
  }

  private UserLoginRequest createLoginRequest(String username, String password) {
    return UserLoginRequest.newBuilder()
      .setName(username)
      .setPassword(password)
      .build();
  }

  private void displayLoginError() {
    Platform.runLater(() -> {
      passwordField.setText(null);
      statusMessage.setTextAlignment(TextAlignment.CENTER);
      statusMessage.setText("Invalid password or username!");
      statusMessage.setVisible(true);
    });
  }

  private boolean invalidTextField(TextField field) {
    return field.getText() == null || field.getText().isEmpty();
  }

  @FXML
  private void signUp() {
    if (invalidTextField(usernameField) || invalidTextField(passwordField)) {
      return;
    }
    String username = usernameField.getText();
    String password = passwordField.getText();

    UserRegistrationRequest request = createRegistrationRequest(username, password);
    ChatClientApplication.serviceStub().register(request, createResponseObserver(username));
  }

  private StreamObserver<UserRegistrationResponse> createResponseObserver(String username) {
    Consumer<UserRegistrationResponse> responseConsumer = createRegistrationResponseConsumer(username);
    return  Observers.ignoreCompletion(responseConsumer, Throwable::printStackTrace);
  }

  private Consumer<UserRegistrationResponse> createRegistrationResponseConsumer(String username) {
    return response -> receiveRegistrationResponse(username, response);
  }

  private UserRegistrationRequest createRegistrationRequest(String username, String password) {
    return UserRegistrationRequest.newBuilder()
      .setName(username)
      .setPassword(password)
      .build();
  }

  @FXML
  private void loginKeyPressed(KeyEvent keyEvent) {
    if (keyEvent.getCode() == KeyCode.ENTER) {
      login();
    }
  }

  private void receiveRegistrationResponse(String username, UserRegistrationResponse response) {
    if (response.getErrorState() == ErrorState.SUCCESS) {
      ChatClientApplication.changeCurrentName(username);
      ChatClientApplication.chats().open();
    } else if (response.getErrorState() == ErrorState.ERROR) {
      displayRegistrationError();
    }
  }

  private void displayRegistrationError() {
    Platform.runLater(() -> {
      statusMessage.setTextAlignment(TextAlignment.CENTER);
      statusMessage.setText("This user already exists!");
      statusMessage.setStyle("-fx-text-fill: red;");
      statusMessage.setVisible(true);
      this.usernameField.setText(null);
    });
  }

  @FXML
  public void moveToRegister(MouseEvent mouseEvent) {
    signUpButton.setStyle("-fx-background-color: #27ae60");
  }

  @FXML
  public void exitFromRegister(MouseEvent mouseEvent) {
    signUpButton.setStyle("-fx-background-color: #2ecc71");
  }

  @FXML
  public void moveToLogin(MouseEvent mouseEvent) {
    loginButton.setStyle("-fx-background-color: #27ae60");
  }

  @FXML
  public void exitFromLogin(MouseEvent mouseEvent) {
    loginButton.setStyle("-fx-background-color: #2ecc71");
  }
}