package de.dvelop.chat.client.chat;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import io.grpc.stub.StreamObserver;

import de.dvelop.chat.ChatMessage;
import de.dvelop.chat.ChatMessageFromServer;
import de.dvelop.chat.ChatServiceGrpc;
import de.dvelop.chat.client.ChatClientApplication;
import de.dvelop.chat.client.Observers;

import javax.sound.sampled.*;

public final class ChatController {

  private static final DateFormat FORMAT = new SimpleDateFormat("dd.MM.yyyy - HH:mm:ss");

  private ObservableList<String> messages = FXCollections.observableArrayList();
  private ListView<String> messagesView = new ListView<>();
  private TextField message = new TextField();
  private Button send = new Button("Send");
  private Stage primaryStage;
  private Clip soundClip;

  public ChatController(Stage primaryStage) throws UnsupportedAudioFileException, IOException, LineUnavailableException {
    this.primaryStage = primaryStage;
    configureSound();
  }

  private void configureSound() throws IOException, UnsupportedAudioFileException, LineUnavailableException {
    File glocke = new File("glocke.wav");
    System.out.println(glocke.getName());
    AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(glocke);

    soundClip = AudioSystem.getClip();
    soundClip.open(audioInputStream);
  }

  public void open() {
    registerChatEvents(createObserver(ChatClientApplication.serviceStub()));
    Platform.runLater(() -> {
      Stage stage = new Stage();
      messagesView.setItems(messages);
      send.setCursor(Cursor.HAND);

      BorderPane pane = new BorderPane();
      pane.setCenter(message);
      pane.setRight(send);

      BorderPane root = new BorderPane();
      root.setCenter(messagesView);
      root.setBottom(pane);

      stage.setTitle("d.velop ChatService");
      stage.setScene(new Scene(root, 480, 320));

      primaryStage.hide();
      stage.show();
    });
  }

  private void receiveMessageFromServer(ChatMessageFromServer messageFromServer) {
    Platform.runLater(() -> {
      String author = messageFromServer.getAuthor();
      String message = messageFromServer.getMessage();
      messages.add("[" + formattedTime() + "] " + author + ": " + message);
      if (!author.equalsIgnoreCase(ChatClientApplication.currentName())) {
        playSound();
      }
    });
  }

  private void playSound() {
    soundClip.stop();
    soundClip.setMicrosecondPosition(0);
    soundClip.start();
  }

  private StreamObserver<ChatMessage> createObserver(ChatServiceGrpc.ChatServiceStub stub) {
    return stub.chat(Observers.ignoreCompletion(this::receiveMessageFromServer, Throwable::printStackTrace));
  }

  private void registerChatEvents(StreamObserver<ChatMessage> observer) {
    message.setOnKeyPressed(keyPressed -> {
      if (keyPressed.getCode().getName().equalsIgnoreCase("Enter")) {
        sendMessageToObserver(observer);
      }
    });
    send.setOnAction(action -> sendMessageToObserver(observer));
  }

  private String formattedTime() {
    return FORMAT.format(new Date());
  }

  private void sendMessageToObserver(StreamObserver<ChatMessage> observer) {
    String message = this.message.getText();
    String name = ChatClientApplication.currentName();
    if (!validEntry(message)) {
      return;
    }
    clearText(this.message);
    observer.onNext(createChatMessage(name, message));
  }

  private ChatMessage createChatMessage(String author, String message) {
    return ChatMessage.newBuilder()
      .setFrom(author)
      .setMessage(message)
      .build();
  }
  private void clearText(TextField field) {
    field.setText(null);
  }

  private boolean validEntry(String entry) {
    return entry != null && !entry.isEmpty();
  }
}