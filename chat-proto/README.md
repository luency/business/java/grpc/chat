[<img src="https://www.holstein-kiel.de/wp-content/uploads/contao_import/sponsoren/logos/dve_Wortmarke_anthrazit(1).png" alt="drawing" width="500"></img>](https://grpc.io/)

# [Chat](https://gitlab.com/luency/business/java/grpc/chat/) - Proto
The Chat's base including all [**gRPC**](https://grpc.io/) requirements.
Compiling by using the [protoc](https://developers.google.com/protocol-buffers) compiler
generates code that is readable by Java.

## Purpose
This artifact is mainly responsible for implementing [gRPC](https://grpc.io/) into your
service or client. It's like an api which you can call different
methods you have created in `ChatService.proto`.

## Installation
**Maven: pom.xml**
```xml
<dependency>
   <groupId>de.dvelop</groupId>
   <artifactId>chat-proto</artifactId>
   <version>1.0-SNAPSHOT</version>
   <scope>compile</scope>
</dependency>
```

**Gradle: build.gradle**
```gradle
  compile group: 'de.dvelop', name: 'chat-proto', version: '1.0-SNAPSHOT'
```