package de.dvelop.chat.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChatServiceApplication {

  public static void main(String[] arguments) {
    SpringApplication.run(ChatServiceApplication.class, arguments);
  }
}