package de.dvelop.chat.service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.MessageDigestPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.logging.Logger;

@Configuration
public class ChatServiceConfiguration {

  @Bean
  public PasswordEncoder encoder() {
    return new MessageDigestPasswordEncoder("SHA-512");
  }

  @Bean
  public Logger logger() {
    return Logger.getLogger("Chat-Service");
  }
}
