package de.dvelop.chat.service.user;

import com.google.common.collect.Sets;

import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.function.Consumer;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

import org.lognet.springboot.grpc.GRpcService;

import io.grpc.stub.StreamObserver;

import de.dvelop.chat.*;
import de.dvelop.chat.service.util.Observers;

@GRpcService
public final class ChatServiceImpl extends ChatServiceGrpc.ChatServiceImplBase {

  private static LinkedHashSet<StreamObserver<ChatMessageFromServer>> observers = Sets.newLinkedHashSet();

  private final UserRepository users;
  private final PasswordEncoder passwords;

  @Autowired
  private ChatServiceImpl(
    UserRepository users,
    PasswordEncoder passwords
  ) {
    this.users = users;
    this.passwords = passwords;
  }

  @Override
  public StreamObserver<ChatMessage> chat(StreamObserver<ChatMessageFromServer> responseObserver) {
    observers.add(responseObserver);
    return createChatObserver(responseObserver);
  }

  @Override
  public void login(
    UserLoginRequest request,
    StreamObserver<UserLoginResponse> responseObserver
  ) {
    String name = request.getName();
    Optional<User> userLookup = users.findByName(name);
    if (!userLookup.isPresent()) {
      rejectLogin(responseObserver);
      return;
    }
    User user = userLookup.get();
    String password = request.getPassword();
    tryToLogin(user, password, responseObserver);
  }

  private void rejectLogin(StreamObserver<UserLoginResponse> responseObserver) {
    responseObserver.onNext(UserLoginResponse.newBuilder().setErrorState(ErrorState.ERROR).build());
    responseObserver.onCompleted();
  }

  private void acceptLogin(StreamObserver<UserLoginResponse> responseObserver) {
    UserLoginResponse response = UserLoginResponse.newBuilder()
      .setErrorState(ErrorState.SUCCESS)
      .build();
    responseObserver.onNext(response);
    responseObserver.onCompleted();
  }

  private void tryToLogin(
    User user,
    String password,
    StreamObserver<UserLoginResponse> responseObserver
  ) {
    if (passwordMatches(user, password)) {
      acceptLogin(responseObserver);
    } else {
      rejectLogin(responseObserver);
    }
  }

  private boolean passwordMatches(User user, String password) {
    return passwords.matches(password, user.encodedPassword());
  }

  private StreamObserver<ChatMessage> createChatObserver(
    StreamObserver<ChatMessageFromServer> responseObserver
  ) {
    Consumer<Throwable> failureConsumer = failure -> applyErrorOnObserver(responseObserver, failure);
    Runnable completion = () -> observers.remove(responseObserver);
    return Observers.create(this::broadcastMessage, failureConsumer, completion);
  }

  private void applyErrorOnObserver(StreamObserver<?> observer, Throwable failure) {
    observers.remove(observer);
    observer.onError(failure);
  }

  @Override
  public void register(
    UserRegistrationRequest request,
    StreamObserver<UserRegistrationResponse> responseObserver
  ) {
    String name = request.getName();
    if (users.existsByName(name)) {
      rejectRegistration(responseObserver);
      return;
    }
    String password = encodePassword(request.getPassword());
    User user = User.create(name, password);
    tryToRegisterUser(user, responseObserver);
  }

  private void tryToRegisterUser(
    User user,
    StreamObserver<UserRegistrationResponse> responseObserver
  ) {
    try {
      users.save(user);
      acceptRegistration(responseObserver);
    } catch (Exception failure) {
      failure.printStackTrace();
      rejectRegistration(responseObserver);
    }
  }

  private String encodePassword(String password) {
    return passwords.encode(password);
  }

  private void acceptRegistration(StreamObserver<UserRegistrationResponse> responseObserver) {
    responseObserver.onNext(UserRegistrationResponse.newBuilder().setErrorState(ErrorState.SUCCESS).build());
    responseObserver.onCompleted();
  }

  private void rejectRegistration(StreamObserver<UserRegistrationResponse> responseObserver) {
    responseObserver.onNext(UserRegistrationResponse.newBuilder().setErrorState(ErrorState.ERROR).build());
    responseObserver.onCompleted();
  }

  private void broadcastMessage(ChatMessage chatMessage) {
    observers.forEach(observer -> transmitMessage(chatMessage, observer));
  }

  private void transmitMessage(
    ChatMessage chatMessage,
    StreamObserver<ChatMessageFromServer> observer
  ) {
    String author = chatMessage.getFrom();
    String message = chatMessage.getMessage();
    ChatMessageFromServer fromServer = createChatMessageFromServer(author, message);
    observer.onNext(fromServer);
  }

  private ChatMessageFromServer createChatMessageFromServer(String author, String message) {
    return ChatMessageFromServer.newBuilder()
      .setAuthor(author)
      .setMessage(message)
      .build();
  }
}