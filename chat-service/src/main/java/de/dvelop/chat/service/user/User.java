package de.dvelop.chat.service.user;

import com.google.common.base.Preconditions;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

@Entity(name = "users")
public final class User implements Serializable {

  @Id
  @GeneratedValue(generator = "increment")
  @GenericGenerator(name = "increment", strategy = "increment")
  private long id;

  @Column(nullable = false, unique = true, length = 20, updatable = false)
  private String name;

  @Column(nullable = false, updatable = false, length = 200, name = "password")
  private String encodedPassword;

  private User(String name, String encodedPassword) {
    this.name = name;
    this.encodedPassword = encodedPassword;
  }

  private User() {}

  public String name() {
    return name;
  }

  public String encodedPassword() {
    return encodedPassword;
  }

  public static User create(String name, String password) {
    Preconditions.checkNotNull(name);
    Preconditions.checkNotNull(password);
    return new User(name, password);
  }
}