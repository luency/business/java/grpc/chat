[<img src="https://www.holstein-kiel.de/wp-content/uploads/contao_import/sponsoren/logos/dve_Wortmarke_anthrazit(1).png" alt="drawing" width="500"></img>](https://grpc.io/)

# [Chat](https://gitlab.com/luency/business/java/grpc/chat/) - Service
The service provides the master to connect to.
Each client must connect on this instance and is verified through it.
This instance also handles incoming requests and manages them. Methods on this
instance can be called through incoming rpc requests from a [client](https://gitlab.com/luency/business/java/grpc/chat/tree/develop/chat-client).
It's connected to a database and also handles user data. It's a typical master.

# Installation

Since this project has not been completed yet, you cannot install it.
However, for testing purposes you can edit the application.properties file to use the service.